﻿using System;
using System.Collections.Generic;
using System.IO.Packaging;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Threading;

namespace TestWorkGame
{
    /// <summary>
    /// Логика взаимодействия для GameWindow.xaml
    /// </summary>
    public partial class GameWindow
    {
        private List<int>  _indexesofnumber=new List<int>();
        private readonly Random _random = new Random();
        private IEnumerable<string> _enumnumbers;
        private int _numberofretries;
        private string _guessnumber;
        private int _numberOfBulls;
        private int _numberOfCows;
        private IList<string> _formattednumbers;
        private string _selectednumber;
        private int _randomindex;
        private int _currentnumberofBulls;
        private int _currentnumberofCows;
        private bool _ischanged;
        public GameWindow()
        {
            InitializeComponent();
        }

        //кнопка 'выход' в режиме 'Угадывает компьютер'
        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        //кнопка 'главное меню' в режиме 'Угадывает компьютер'
        private void MainMenuButton_Click(object sender, RoutedEventArgs e)
        {
            GoToMainMenu();
        }

        //кнопка 'включить дополнительные возможности' в режиме 'Угадывает компьютер'
        private void ExtraFeaturesButton_Checked(object sender, RoutedEventArgs e)
        {
            ExtraFeaturesButton.IsEnabled = false;
            EnableGroupBox(ExtraFeatures);
            OnOrOffSomeExtraFeatures(false);
        }

        //кнопка 'новая игра' в режиме 'Угадывает компьютер'
        private void PcGuessNewGameButton_Click(object sender, RoutedEventArgs e)
        {
            #region OnOffSomeElements
            //PcIntellectGuessNextStepButton.Visibility=Visibility.Hidden;
            PcGuessPcMindsTextBox.Clear();
            PcGuessHelloTextBox.IsEnabled = false;
            SomeInfoButton.IsEnabled = false;
            ExtraFeaturesButton.IsEnabled = false;
            PcGuessNewGameButton.IsEnabled = false;
            OnOrOffSomeExtraFeatures(true);
            HumanGuess.IsEnabled = false;
            PcGuessNextStepButton.IsEnabled = true;
            PCsMind.IsEnabled = true;
            PCsMind.Foreground = Brushes.Black;
            PCsMind.BorderBrush = Brushes.Black;
            PcGuessPcMindsTextBox.IsEnabled = true;
            PcGuessEnterNumberLabel.IsEnabled = true;
            PcGuessEnterNumberTextBox.IsEnabled = true;
            #endregion
            CreatingListofCorrectNumbers(out _formattednumbers);
            _numberOfBulls = 0;
            _numberOfCows = 0;
            _numberofretries = 0;

        }

        //кнопка 'далее' в режиме 'Угадывает компьютер'
        private void PcGuessNextStepButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PcGuessEnterNumberTextBox.IsEnabled)
                {
                    _selectednumber = PcGuessEnterNumberTextBox.Text;
                }
                //создаем вот такое гигантское условие для ввода человеком правильного числа
                if ((Convert.ToInt32(_selectednumber) >= 1234) && (Convert.ToInt32(_selectednumber) <= 9876)
                    && (_selectednumber[0] != _selectednumber[1]) && (_selectednumber[0] != _selectednumber[2])
                    && (_selectednumber[0] != _selectednumber[3]) && (_selectednumber[1] != _selectednumber[2])
                    && (_selectednumber[1] != _selectednumber[3]) && (_selectednumber[2] != _selectednumber[3])
                    && (_selectednumber[0] != '0'))
                {
                    //небольшая магия с окошками
                    if (PcGuessEnterNumberTextBox.IsEnabled)
                    {
                        PcGuessYourNumberTextBox.Text = PcGuessEnterNumberTextBox.Text;
                        PcGuessEnterNumberTextBox.Clear();
                        PcGuessEnterNumberTextBox.IsEnabled = false;
                        PcGuessEnterNumberLabel.IsEnabled = false;
                    }
                    //Смотрим,какой тип задачи мы выбрали
                    if (OrderChooseRadioButton.IsChecked == true)
                    {
                        //вызываем соответствующий метод и передаем число попыток,ибо мы угадываем по порядку
                        PcGuessMethodForOrderAndRandomStyle(_numberofretries);
                    }
                    else if (RandomChooseRadioButton.IsChecked == true)
                    {
                        //вызываем соответствующий метод и передаем рандомно сгенерированное число, причем учитываются удаленные позже из списка числа
                        _randomindex = _random.Next(0, _formattednumbers.Count() - 1);
                        PcGuessMethodForOrderAndRandomStyle(_randomindex);
                        _formattednumbers.RemoveAt(_randomindex);

                    }
                    else if (IntellectChooseRadioButton.IsChecked == true)
                    {
                        //запускаем умного бота
                        MethodForIntellectBot();
                    }
                    else
                    {
                        MethodForIntellectBot();//если не выбран стиль,то по умолчанию выбираем умного бота
                        IntellectPreGameActions();//выключаем неужные окошки
}
                    
                }
                else
                    throw new ArgumentException(
                        "Число должно быть 4 -х значным ,не должно содержать одинаковых цифр,и первая цифра не может быть 0!!!");
            }
            
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK);
            }
            catch (FormatException)
            {
                MessageBox.Show("Нельзя вводить буквы или другие знаки, кроме цифр!!!", "Ошибка", MessageBoxButton.OK);
            }
        }

        //включаем или выключаем некоторые элементы группы 'Дополнительные возможности'
        private void OnOrOffSomeExtraFeatures(bool value)
        {
            PcGuessCowCalculateLabel.IsEnabled = value;
            PcGuessCowCalculateTextBox.IsEnabled = value;
            PcGuessBullCalculateLabel.IsEnabled = value;
            PcGuessBullCalculateTextBox.IsEnabled = value;
            PcGuessYourNumberLabel.IsEnabled = value;
            PcGuessYourNumberTextBox.IsEnabled = value;
            PcGuessGuessedNumberLabel.IsEnabled = value;
            PcGuessGuessedNumberTextBox.IsEnabled = value;
            CalculateButton.IsEnabled = value;
            if (!value)
            {
                NumberGuessStyle.IsEnabled = true;
                NumberGuessStyle.Foreground = Brushes.Black;
                NumberGuessStyle.BorderBrush = Brushes.Black;
            }
            else
            {
                NumberGuessStyle.IsEnabled = false;
                NumberGuessStyle.Foreground = Brushes.Gray;
                NumberGuessStyle.BorderBrush = Brushes.LightBlue;
            }

        }

        //методы угадывания числа компьютером для рандомного стиля и стиля по порядку
        private void PcGuessMethodForOrderAndRandomStyle(int index)
        {
            try
            {
                if (PcGuessCowBox.IsEnabled)
                {
                    _numberOfBulls = Convert.ToInt32(PcGuessBullTextBox.Text);
                    _numberOfCows = Convert.ToInt32(PcGuessCowBox.Text);
                    if (
                        !((_numberOfBulls >= 0) && (_numberOfBulls <= 4) && (_numberOfCows >= 0) &&
                          (_numberOfBulls <= 4)))
                        throw new ArgumentException("Число коров и быков должно быть от 0 до 4!!!");
                }
                if ((_numberOfCows == 0) && (_numberOfBulls == 4))
                {
                    if (_selectednumber == _formattednumbers[index - 1])
                    {
                        MessageBox.Show("Игра завершена!", "Конец игры", MessageBoxButton.OK);
                        PcGuessPcMindsTextBox.Text += "Число затраченных попыток: " + index +
                                                      Environment.NewLine;
                        PostGameClear();
                    }
                    else
                    {
                        MessageBox.Show(
                            "Не Не Не! Введенное вами число и предложенное компом число не совпадают! Вы где-то ошиблись с числом быков и коров",
                            "Ошибка",
                            MessageBoxButton.OK);
                        PostGameClear();
                    }
                }
                else
                {
                    if ((PcGuessCowBox.Text == "") && (PcGuessBullTextBox.Text == "") && (_numberofretries != 0))
                        return;
                    PcGuessPcMindsTextBox.Text += "Ваше число: " + _formattednumbers[index] + "?" +
                                                  Environment.NewLine;
                    if (_numberofretries == 0)
                    {
                        PcGuessEnterBullAndCowLabel.IsEnabled = true;
                        PcGuessBullLabel.IsEnabled = true;
                        PcGuessBullTextBox.IsEnabled = true;
                        PcGuessCowBox.IsEnabled = true;
                        PcGuessCowLabel.IsEnabled = true;
                    }
                    _numberofretries++;
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK);
            }
            catch (FormatException)
            {
                MessageBox.Show("Нельзя вводить буквы или другие знаки, кроме цифр", "Ошибка", MessageBoxButton.OK);
            }

        }

        //подсчитывает число быков и коров для заданных двух чисел
        private void CalculateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if ((PcGuessGuessedNumberTextBox.Text != "") && (Convert.ToInt32(PcGuessGuessedNumberTextBox.Text) >= 1234)
                    && (Convert.ToInt32(PcGuessGuessedNumberTextBox.Text) <= 9876))
                {
                    //если введем буквы,возбудим FormatException
                    var int32 = Convert.ToInt32(PcGuessGuessedNumberTextBox.Text);
                    //простая чистка блоков
                    PcGuessBullCalculateTextBox.Clear();
                    PcGuessCowCalculateTextBox.Clear();
                    _selectednumber = PcGuessYourNumberTextBox.Text;
                    _guessnumber = PcGuessGuessedNumberTextBox.Text;
                    //вычисляем быков и коров и выводим результат
                    CalculateNumberofBullsAndCows(out _numberOfBulls, out _numberOfCows);
                    PcGuessBullCalculateTextBox.Text += _numberOfBulls;
                    PcGuessCowCalculateTextBox.Text += _numberOfCows;
                }
                else throw new ArgumentNullException();
            }
            catch (ArgumentNullException)
            {
                MessageBox.Show("Необходимо ввести число, предложенное компьютером", "Ошибка", MessageBoxButton.OK);
            }
            catch (FormatException)
            {
                MessageBox.Show("Нельзя вводить буквы или другие знаки, кроме цифр!!!", "Ошибка", MessageBoxButton.OK);
            }
        }

        //кнопка 'немного информации' в режиме 'Угадывает компьютер'
        private void SomeInfoButton_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Рекомендуем использовать дополнительные параметры для выбора стиля угадывания числа,а также чтобы проверить себя на количество быков и коров"
                + Environment.NewLine + "Также рекомендуем использовать только стиль угадывания 'Интеллектуально', поскольку два других режима крайне тупые,и игра будет длиться целую вечность"
                + Environment.NewLine + "!!!Внимание!!!"
                + Environment.NewLine + "По умолчанию стиль угадывания:'Интеллектуально'"
                +Environment.NewLine+"Также при работе в режиме 'Интеллектуально' каждый раз в момент, когда компьютер предлагает число, будет вылезать отдельное окно, где вы сможете указать количество быков и коров, а также для самопроверки ввести предложенное компьютером число в калькулятор","Информация, которую надо прочесть перед началом игры");

        }

        //кнопка 'новая игра' в режиме 'Угадывает человек'
        private void HumanGuessNewGameButton_Click(object sender, RoutedEventArgs e)
        {
            HumanPreGameActions();
            ProgressLabel.Content = "Введите 4-х значное число!";
            //создаем отфильтрованный список чисел ,и выбираем рандомное число,ну и вытыскиваем их из метода
            CreatingListofCorrectNumbers(out _formattednumbers);
            //компьютер выбирает случайное число из отфильтрованных нами ранее
            _selectednumber = _formattednumbers[_random.Next(0, _formattednumbers.Count() - 1)];
            //List<string> list = _formattednumbers.ToList();
            HumanGuessHistoryTextBox.Text += "Я загадал число..." + "   Попробуйте его угадать :)" + Environment.NewLine +
                                             "----------------------------" + Environment.NewLine;
        }

        //кнопка 'главное' в режиме 'Угадывает человек'
        private void HumanGuessMainMenuButton_Click(object sender, RoutedEventArgs e)
        {
            GoToMainMenu();
        }

        //кнопка 'далее' в режиме 'Угадывает человек'
        private void HumanGuessNextStepButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                _numberofretries++;
                HumanGuessBullBox.Clear();
                HumanGuessCowBox.Clear();
                //считываем введенное человеком число
                _guessnumber = HumanGuessEnterNumberBox.Text;
                if ((Convert.ToInt32(_guessnumber) >= 1234) && (Convert.ToInt32(_guessnumber) <= 9876)
                     && (_guessnumber[0] != _guessnumber[1]) && (_guessnumber[0] != _guessnumber[2])
                     && (_guessnumber[0] != _guessnumber[3]) && (_guessnumber[1] != _guessnumber[2])
                     && (_guessnumber[1] != _guessnumber[3]) && (_guessnumber[2] != _guessnumber[3])&&(_guessnumber[0]!='0'))
                {
                    ProgressTextBox.Text += "Попытка " + _numberofretries + ":";
                    //если оно совпало, то заканчиваем игру, чистим переменные и отключаем окошки
                    if (_guessnumber == _selectednumber)
                    {
                        HumanGuessBullBox.IsEnabled = false;
                        HumanGuessCowBox.IsEnabled = false;
                        ProgressTextBox.Text += " Успех!!!" + Environment.NewLine + "Число затраченных попыток: " +
                                                _numberofretries;
                        HumanGuessHistoryTextBox.Text += "Вы угадали! Это было число " + _selectednumber +
                                                         Environment.NewLine;
                        MessageBox.Show("Поздравляю! Вы угадали число.Подробности описаны во вкладке 'История'",
                            "Конец игры");
                        ProgressLabel.Content = "Игра завершена!";
                        HumanGuessNextStepButton.IsEnabled = false;
                        HumanGuessEnterNumberBox.IsEnabled = false;
                        HumanGuessEnterNumberLabel.IsEnabled = false;
                        HumanGuessNewGameButton.IsEnabled = true;
                        PcGuess.IsEnabled = true;
                        _numberofretries = 0;
                        _numberOfBulls = 0;
                        _numberOfCows = 0;
                    }
                    else
                    {
                        ProgressTextBox.Text += " Неудача!" + Environment.NewLine;
                        HumanGuessHistoryTextBox.Text +=
                            "Неа! Не то число! Но я могу сказать вам сколько в нем быков и коров" + Environment.NewLine +
                            "----------------------------" + Environment.NewLine;
                        //собственно подсчитываем сколько быков и коров
                        CalculateNumberofBullsAndCows(out _numberOfBulls, out _numberOfCows);
                        //и выводим их
                        HumanGuessBullBox.Text += _numberOfBulls;
                        HumanGuessCowBox.Text += _numberOfCows;

                    }
                }
                else throw new ArgumentException("Число должно быть 4 -х значным ,не должно содержать одинаковых цифр,и первая цифра не может быть 0!!!");
            }
            catch (FormatException)
            {
                _numberofretries--;
                MessageBox.Show("Необходимо ввести именно 4-х значное число!", "Ошибка", MessageBoxButton.OK);
            }
            catch (ArgumentException ex)
            {
                _numberofretries--;
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButton.OK);
            }

        }

        //кнопка 'выход' в режиме 'Угадывает человек'
        private void HumanGuessExitButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        
        //отключение окошек и прочего, чистка переменных и боксов
        private void PostGameClear()
        {
            PcGuessCowBox.Clear();
            PcGuessCowBox.IsEnabled = false;
            PcGuessCowLabel.IsEnabled = false;
            PcGuessBullTextBox.Clear();
            PcGuessBullTextBox.IsEnabled = false;
            PcGuessBullLabel.IsEnabled = false;
            PcGuessEnterBullAndCowLabel.IsEnabled = false;
            PcGuessNextStepButton.IsEnabled = false;
            PcGuessNewGameButton.IsEnabled = true;
            SomeInfoButton.IsEnabled = true;
            PcGuessHelloTextBox.IsEnabled = true;
            ExtraFeaturesButton.IsEnabled = true;
            ExtraFeaturesButton.IsChecked = false;
            ExtraFeatures.IsEnabled = false;
            ExtraFeatures.Foreground = Brushes.Gray;
            HumanGuess.IsEnabled = true;
            OrderChooseRadioButton.IsChecked = false;
            RandomChooseRadioButton.IsChecked = false;
            IntellectChooseRadioButton.IsChecked = false;
            _numberOfCows = 0;
            _numberOfBulls = 0;
            _numberofretries = 0;
            _ischanged = false;
            PcGuessYourNumberTextBox.Clear();
            PcGuessGuessedNumberTextBox.Clear();
            PcGuessCowCalculateTextBox.Clear();
            PcGuessBullCalculateTextBox.Clear();
        }

        //нахождение числа коров и быков компьютером
        private void CalculateNumberofBullsAndCows(out int numberOfBulls, out int numberOfCows)
        {
            numberOfBulls = 0;
            numberOfCows = 0;
            //сравниваем каждую цифру одного числа с каждой цифрой другого числа
            //если какие-то совпали,то условие: если и позиция совпала, то это бык, иначе корова
            for (int i = 0; i < _selectednumber.Count(); i++)
            {
                for (int j = 0; j < _guessnumber.Count(); j++)
                {
                    if (_selectednumber[i] == _guessnumber[j])
                    {
                        if (i == j)
                        {
                            numberOfBulls++;
                        }
                        else numberOfCows++;
                    }
                }
            }
        }

        private void GoToMainMenu()
        {
            MainMenu window = new MainMenu();
            Close();
            window.Show();
        }

        //создаем список нужных по условию чисел
        private void CreatingListofCorrectNumbers(out IList<string> formattednumbers)
        {
            var numbers = new List<string>();
            //создаем список всех чисел от 1234 до 9876
            for (int i = 1234; i <= 9876; i++)
            {
                numbers.Add(i.ToString());
            }
            //создаем linq-запрос и фильтруем числа от 1234 до 9876 так,чтобы все цифры были разные
            _enumnumbers = from number in numbers
                           where
                               ((number[0] != number[1]) && (number[0] != number[2]) && (number[0] != number[3]) &&
                                (number[1] != number[2]) && (number[1] != number[3]) && (number[2] != number[3]))
                           select number;
            //преобразуем enumerable список в Ilist,чтобы мы потом могли спокойно по индексу выбрать случайный элемент
            formattednumbers = _enumnumbers as IList<string> ?? _enumnumbers.ToList();
        }

        //тут по сути тупо включаем и отключаем отдельные элементы интефейса для вкладки "Угадывает человек"
        private void HumanPreGameActions()
        {
            HumanGuessBullBox.Clear();
            HumanGuessCowBox.Clear();
            HumanGuessHistoryTextBox.Clear();
            ProgressTextBox.Clear();
            HumanGuessBullBox.IsEnabled = true;
            HumanGuessCowBox.IsEnabled = true;
            HumanGuessHelloBox.IsEnabled = false;
            PcGuess.IsEnabled = false;
            EnableGroupBox(HumanGuessPcGroupBox);
            HumanGuessEnterNumberLabel.IsEnabled = true;
            HumanGuessEnterNumberBox.IsEnabled = true;
            HumanGuessNewGameButton.IsEnabled = false;
            HumanGuessNextStepButton.IsEnabled = true;
            ProgressTextBox.IsEnabled = true;
            ProgressLabel.IsEnabled = true;
        }

        //включаем соответствующую группу элементов
        private void EnableGroupBox(GroupBox gpBox)
        {
            gpBox.IsEnabled = true;
            gpBox.BorderBrush = Brushes.Black;
            gpBox.Foreground = Brushes.Black;
        }

        #region Умный бот
        //метод, запускающий умного бота
        private void MethodForIntellectBot()
        {
            CreatingListofCorrectNumbers(out _formattednumbers);
            _randomindex = _random.Next(0, _formattednumbers.Count() - 1);
            _guessnumber = _formattednumbers[_randomindex];
            while (_guessnumber != _selectednumber)
            {
                if (_numberofretries == 0)
                {
                    PcGuessPcMindsTextBox.Text += "Ваше число: " + _guessnumber + Environment.NewLine;
                    _numberofretries++;
                    InitializingIntellectEnterofDataAndReadingData();
                }
                else
                {
                    //смотрим на всевозможные комбинации числа быков и коров 
                    //и вызываем соответствующих данной комбинации метод
                    if ((_numberOfBulls == 0) && (_numberOfCows == 0) || (_numberOfBulls == 0) && (_numberOfCows == 1) ||
                        (_numberOfBulls == 1) && (_numberOfCows == 0))
                    {
                        _formattednumbers.RemoveAt(_randomindex);
                        _randomindex = _random.Next(0, _formattednumbers.Count() - 1);
                        _guessnumber = _formattednumbers[_randomindex];
                        PcGuessPcMindsTextBox.Text += "Ваше число: " + _guessnumber + Environment.NewLine;
                        InitializingIntellectEnterofDataAndReadingData();
                        _numberofretries++;
                    }
                    if ((_numberOfBulls == 0) && (_numberOfCows == 2))
                    {
                        SwapCicleFor2BullsAnd0Cows(-2, _numberOfBulls + _numberOfCows);
                    }
                    if ((_numberOfBulls == 0) && (_numberOfCows == 3))
                    {
                        SwapCicleFor2BullsAnd1Cows(-2);
                    }
                    if ((_numberOfBulls == 0) && (_numberOfCows == 4))
                    {
                        SwapCicleFor2BullsAnd0Cows(-2, _numberOfBulls + _numberOfCows - 2);
                    }
                    if ((_numberOfBulls == 1) && (_numberOfCows == 1))
                    {
                        MessageBox.Show("Вошли в 1 1");
                        SwapCicleFor2BullsAnd0Cows(-2, _numberOfBulls + _numberOfCows);
                    }
                    if ((_numberOfBulls == 1) && (_numberOfCows == 2))
                    {
                        SwapCicleFor2BullsAnd1Cows(-2);
                    }
                    if ((_numberOfBulls == 1) && (_numberOfCows == 3))
                    {
                        SwapCicleFor2BullsAnd0Cows(-2, _numberOfBulls + _numberOfCows - 2);
                    }
                    if ((_numberOfBulls == 2) && (_numberOfCows == 0))
                    {
                        SwapCicleFor2BullsAnd0Cows(2, _numberOfBulls + _numberOfCows);
                    }
                    if ((_numberOfBulls == 2) && (_numberOfCows == 1))
                    {
                        SwapCicleFor2BullsAnd1Cows(2);
                    }
                    if ((_numberOfBulls == 2) && (_numberOfCows == 2))
                    {
                        SwapCicleFor2BullsAnd0Cows(2, _numberOfBulls + _numberOfCows - 2);
                    }
                    if (_numberOfBulls == 3)
                    {
                        SwapCicleFor2BullsAnd1Cows(2);
                    }
                    //конец игры
                    if (_numberOfBulls == 4)
                    {
                        MessageBox.Show("Компьютер угадал число за " + _numberofretries + " попыток");
                        PcGuessPcMindsTextBox.Text += "Число затраченных попыток: " + _numberofretries + Environment.NewLine;
                        PostGameClear();
                    }
                }
            }
        }

        //рандомим число из отформатированного списка
        private void FinalRandomGuess()
        {
            //если список не содержит чисел то выходим из метода
            if (!_formattednumbers.Any()) return;
            //задаем индекс
            if (_formattednumbers.Count() > 1)
            {
                _randomindex = _random.Next(0, _formattednumbers.Count() - 1);
            }
            else _randomindex = 0;
            //задаем число,удаляем его из списка и спрашиваем о нем у пользователя
            _guessnumber = _formattednumbers[_randomindex];
            _formattednumbers.RemoveAt(_randomindex);
            PcGuessPcMindsTextBox.Text += "Ваше число: " +
                                          _guessnumber + "?" +
                                          Environment.NewLine;
            _numberofretries++;
            InitializingIntellectEnterofDataAndReadingData();
        }

        //форматируем наш список с указанными индексами
        private void FormattingListOfNumbers(int i, int j)
        {
            //делаем LINQ-запрос, где отбираем номера с двумя указанными индексами
            _enumnumbers = from number in _formattednumbers
                           where
                               ((number[i] == _guessnumber[i]) &&
                                (number[j] == _guessnumber[j]))
                           select number;
            //преобразуем его в IList ,чтобы мы могли обращаться к элементам по индексу
            _formattednumbers = _enumnumbers as IList<string> ??
                                _enumnumbers.ToList();
        }

        //цикл для комбинаций 0 3 , 3 0, 1 2, 2 1
        private void SwapCicleFor2BullsAnd1Cows(int value)
        {
            //пока не получили искомое число, будет повторять алгоритм
            while (_numberOfBulls != 4)
            {
                //если список уже отформатирован и найдено 3 быка, то весь цикл мы пропускаем
                if (!_ischanged)
                {
                    //запоминаем текущее количество быков и коров
                    _currentnumberofBulls = _numberOfBulls;
                    _currentnumberofCows = _numberOfCows;
                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = i + 1; j < 4; j++)
                        {
                            if (!_ischanged)
                            {
                                //свапнули две цифры числа и спросили о новом числе у пользователя
                                _guessnumber = SwapElementsofString(i, j, _guessnumber);
                                PcGuessPcMindsTextBox.Text += "Ваше число: " + _guessnumber + "?" + Environment.NewLine;
                                _numberofretries++;
                                InitializingIntellectEnterofDataAndReadingData();
                                //если у нас число быков уменьшилось(увеличилось) на 2, начинаем конечную обработку
                                if (_numberOfBulls == _currentnumberofBulls - value)
                                {
                                    //если у нас изначально было два быка, а стало 0 то свапаем обратно
                                    //в противном случае оставляем как есть,ведь мы только что нашли 2 быков и их позиции
                                    if (value != -2)
                                    {
                                        _guessnumber = SwapElementsofString(i, j, _guessnumber);

                                    }
                                    else
                                    {
                                        //здесь мы запоминаем 3 0, поскольку для дальнейших действий нам нужны именно такие индексы
                                        _currentnumberofBulls = 3;
                                        _currentnumberofCows = 0;
                                    }
                                    //форматируем наш список ппо указанным индексам
                                    FormattingListOfNumbers(i, j);
                                    //далее мы создаем список из индексов числа, после чего мы удаляем из него 2 текущих, и получаем два оставшихся
                                    for (int k = 0; k < 4; k++)
                                    {
                                        _indexesofnumber.Add(k);
                                    }
                                    _indexesofnumber.Remove(i);
                                    _indexesofnumber.Remove(j);
                                    //снова свапаем наше число, но уже используем два оставшихся индекса
                                    //это нужно для того, чтобы правильно впоследствии отформатировать список с 3 быками
                                    _guessnumber = SwapElementsofString(_indexesofnumber[0], _indexesofnumber[1],
                                        _guessnumber);
                                    PcGuessPcMindsTextBox.Text += "Ваше число: " + _guessnumber + "?" + Environment.NewLine;
                                    _numberofretries++;
                                    //ну и спрашиваем про число у пользователя
                                    InitializingIntellectEnterofDataAndReadingData();
                                    //если число быков уменьшилось, тогда свапаем число обратно, ведь число  быков было изначально 3,а мы уменьшили число
                                    if (_numberOfBulls == _currentnumberofBulls - 1)
                                    {
                                        _guessnumber = SwapElementsofString(_indexesofnumber[0], _indexesofnumber[1],
                                        _guessnumber);
                                    }
                                    //далее свапаем индексы i и первый из оставшихся в списке и спрашиваем про число у пользователя
                                    _guessnumber = SwapElementsofString(i, _indexesofnumber[0], _guessnumber);
                                    PcGuessPcMindsTextBox.Text += "Ваше число: " + _guessnumber + "?" + Environment.NewLine;
                                    _numberofretries++;
                                    InitializingIntellectEnterofDataAndReadingData();
                                    //если число быков уменьшилось на два, тогда мы свапаем обратно и форматируем по этим двум цифрам
                                    //задаем _ischanged на true чтобы не пойти опять по циклу
                                    if (_numberOfBulls == _currentnumberofBulls - 2)
                                    {
                                        _guessnumber = SwapElementsofString(i, _indexesofnumber[0], _guessnumber);
                                        FormattingListOfNumbers(i, _indexesofnumber[0]);
                                        _ischanged = true;
                                    }
                                    //иначе мы тоже форматируем список, но уже с i индексом,и оставшимся в списке _indexesofnumber[1]
                                    else
                                    {
                                        _guessnumber = SwapElementsofString(i, _indexesofnumber[0], _guessnumber);
                                        FormattingListOfNumbers(i, _indexesofnumber[1]);
                                        _ischanged = true;
                                    }
                                }
                                //если же число быков увеличилось на 1 и стало равно 3, тогда мы запоминаем текущее количество быков и коров
                                //при value=-2 мы его минусуем чтобы при следующем проходе по циклу мы попали куда надо
                                //и обнуляем индексы цикла,чтобы не доходить до конца цикла, начать все сначала и определить позиции двух быков
                                if (_numberOfBulls == _currentnumberofBulls + 1)
                                {
                                    if (_numberOfBulls == 3)
                                    {
                                        _currentnumberofBulls = 3;
                                        _currentnumberofCows = 0;
                                        if (value == -2) value = -value;
                                        i = 0;
                                        j = 0;
                                    }
                                    //если быков не 3 тогда просто делаем так:
                                    else
                                    {
                                        _currentnumberofBulls++;
                                        _currentnumberofCows--;
                                    }
                                }
                                //в противном случае просто свапаем число обратно
                                else
                                {
                                    _guessnumber = SwapElementsofString(i, j, _guessnumber);
                                }
                            }
                            //если случилось так что мы не попали на ветку,когда число быков больше на 1 и равно 3
                            //тогда мы просто заново идем по циклу
                            if ((i == 2) && (j == 3) && (!_ischanged))
                            {
                                i = 0;
                                j = 1;
                            }
                        }
                    }
                }
                //вызываем метод и рандомим новое число из полностью отформатированного списка
                FinalRandomGuess();
            }
        }

        //цикл для комбинаций 0 2, 0 4, 1 1, 1 3, 2 0, 2 2
        private void SwapCicleFor2BullsAnd0Cows(int value, int bullcowsum)
        {
            //данная переменная необходима нам,чтобы войти вначале в цикл комбинациям 0 4 ,1 3
            while (bullcowsum <= 2)
            {
                //запоминаем текущие позиции
                _currentnumberofBulls = _numberOfBulls;
                _currentnumberofCows = _numberOfCows;
                for (int i = 0; i < 3; i++)
                {
                    for (int j = i + 1; j < 4; j++)
                    {
                        //если список не отформатирован тогда начинаем алгоритм
                        if (!_ischanged)
                        {
                            //свапаем наше число и спрашиваем о нем у пользователя
                            _guessnumber = SwapElementsofString(i, j, _guessnumber);
                            PcGuessPcMindsTextBox.Text += "Ваше число: " + _guessnumber + "?" + Environment.NewLine;
                            _numberofretries++;
                            InitializingIntellectEnterofDataAndReadingData();
                            //если число быков уменьшилось(увеличилось) на 2
                            if (_numberOfBulls == _currentnumberofBulls - value)
                            {
                                //проверяем, все ли цифры предложенного числа есть в загаданном числе
                                if ((_currentnumberofBulls == 2) && (_currentnumberofCows == 2))
                                {
                                    //если число быков уменьшилось на 2 ,а не увеличилось, тогда мы свапаем обратно, иначе оставляем
                                    if (value != -2) _guessnumber = SwapElementsofString(i, j, _guessnumber);
                                    //создаем список индексов чтобы впоследствии правильно свапнуть две другие цифры и спрашиваем про число у пользователя
                                    for (int k = 0; k < 4; k++)
                                    {
                                        _indexesofnumber.Add(k);
                                        MessageBox.Show(k.ToString());
                                    }
                                    _indexesofnumber.Remove(i);
                                    _indexesofnumber.Remove(j);
                                    _guessnumber = SwapElementsofString(_indexesofnumber[0], _indexesofnumber[1],
                                        _guessnumber);
                                    PcGuessPcMindsTextBox.Text += "Ваше число: " + _guessnumber + "?" + Environment.NewLine;
                                    _numberofretries++;
                                    InitializingIntellectEnterofDataAndReadingData();
                                    //если число быков стало равно 2, тогда удаляем и списка все числа,чтобы в конце алгоритма не начать генерировать другие числа
                                    if (_numberOfBulls == 4)
                                    {
                                        _formattednumbers.Clear();
                                    }

                                }
                                //иначе если не все цифры содержатся в числе мы свапаем обратно, когда число быков уменьшилось на 2, в противном случае 
                                //оставляем цифры
                                else if (value != -2)
                                {
                                    _guessnumber = SwapElementsofString(i, j, _guessnumber);
                                }
                                //форматируем список и обновляем значение переменной bullcowsum
                                FormattingListOfNumbers(i, j);
                                _ischanged = true;
                                //bullcowsum = _numberOfBulls + _numberOfCows;
                            }
                            //проверим,поставили ли мы цифру на позицию быка
                            else if ((_numberOfBulls == _currentnumberofBulls + 1) && (value == -2))
                            {
                                //если мы поставили второго быка на место,запоминаем новые индексы и дальше опять будем свапать пока число коров не уменьшиться на 2
                                //чтобы найти позиции индексов и обработать список соответствующим linq запросом

                                if ((_numberOfBulls == 2) && (_numberOfCows == 0))
                                {
                                    //в данном случае мы запоминаем текущее число быков и коров, обнуляем переменные i и j
                                    //чтобы просто заново начать цикл и если у нас число быков изначально было меньше числа коров, тогда 
                                    //у нашей переменной меняем значение на противоположное
                                    _currentnumberofBulls = 2;
                                    _currentnumberofCows = _numberOfCows;
                                    if (value == -2) value = -value;
                                    i = 0;
                                    j = 0;
                                }
                                //если же в числе присутствуют все цифры(комбинация 0 4) ,тогда делаем все то же самое,что в прошлой ветке
                                //вот только мы всегда меняем значение переменной value на противоположное
                                else if ((_numberOfBulls == 2) && (_numberOfCows == 2))
                                {
                                    _currentnumberofBulls = 2;
                                    _currentnumberofCows = 2;
                                    value = -value;
                                    i = 0;
                                    j = 0;
                                }
                                _currentnumberofBulls = _numberOfBulls;
                            }
                            //иначе просто возвращаем цифры на место
                            else
                            {
                                _guessnumber = SwapElementsofString(i, j, _guessnumber);
                            }

                        }
                    }
                }
                //простое угадывание числа
                FinalRandomGuess();
                //переменной bullcowsum присваиваем сумму,чтобы в случае, когда сумма быков и коров была больше двух,
                //мы вышли из цикла и попали на новый соответствующий метод
                bullcowsum = _numberOfBulls + _numberOfCows;

            }
            _ischanged = false;
            //на всякий случай обнулим список
            _indexesofnumber.Clear();
        }

        //данный метод вызывает окно для ввода числа быков и коров,и запоминает их в соответствующие переменные
        private void InitializingIntellectEnterofDataAndReadingData()
        {
            IntellectEnterData window = new IntellectEnterData(_selectednumber, _guessnumber);
            window.ShowDialog();
            _numberOfBulls =
                window.NumberofBulls;
            _numberOfCows =
                window.NumberofCows;

        }

        //свапаем две цифры числа  по  указанным индексам и результатом нашего метода как раз и является наше число
        private static string SwapElementsofString(int i, int j, string guessnumber)
        {
            char[] temp = guessnumber.ToCharArray();
            char ch = temp[i];
            temp[i] = temp[j];
            temp[j] = ch;
            //LINQ-запрос,который строит нашу строку из элементов temp массива
            guessnumber = temp.Aggregate("", (current, c) => current + c);
            return guessnumber;
        }
        //если выбрали интеллектуальным режим угадывания, просто отключаем ненужные кнопки
        private void IntellectChooseRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            IntellectPreGameActions();
        }

        private void IntellectPreGameActions()
        {
            PcGuessCowBox.Clear();
            PcGuessCowBox.IsEnabled = false;
            PcGuessCowLabel.IsEnabled = false;
            PcGuessBullTextBox.Clear();
            PcGuessBullTextBox.IsEnabled = false;
            PcGuessBullLabel.IsEnabled = false;
            ExtraFeatures.IsEnabled = false;
            ExtraFeatures.IsEnabled = false;
            NumberGuessStyle.Foreground = Brushes.Gray;
            NumberGuessStyle.BorderBrush = Brushes.LightBlue;
            ExtraFeatures.Foreground = Brushes.Gray;
            ExtraFeatures.BorderBrush = Brushes.LightBlue;
        }
        #endregion
    }
}
